import React, {Component} from 'react';
import {
   Text,
   StyleSheet,
   ScrollView,
   TouchableOpacity,
   View,
   TextInput,
   Platform,
   ImageBackground,
   Dimensions,
   Image,
} from 'react-native';
import {connect} from 'react-redux';
import MyIcon from 'react-native-vector-icons/Ionicons';
import ScalableText from 'react-native-text';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

const mapStateToProps = (state) => {
 return {

 }
}


const mapDispatchToProps = (dispatch) => {
 return {}
}


class SpaceList extends React.Component {

  static defaultProps={
    TouchableActiveSpaceCircle:{
      flex:1,
      overflow:"hidden",
      borderWidth:1,
      borderColor:"white",
      shadowColor: "#d7d7d7",
      shadowOffset: { "width": 0, "height": 2 },
      shadowOpacity: 1,
      shadowRadius: 12,
      height:width*.19,
      width:width*.18,
      borderRadius:(width*.18/2)
    },
    activeSpaceTopCircle:{},
    circleWidth:width*.30,
    spaceImage:'',
    circleText:{
      color:"black",
      fontSize:12,
      fontFamily : "PingFang SC"
    },
    spaceNumber:'',
    textStyle:{
    fontWeight:"bold",
    fontSize:14,
    fontFamily :"PingFang SC",
    color:"black",
    textAlign:"center"
  },
    imageStyle:{
      "flex":1,
      height:width*.19,
      width:width*.18,
      borderRadius:(width*.18)/2
    },
    containerStyle:{},
    textViewStyle:{
            flex:1,
            justifyContent:"center",
            alignItems:"center",
            marginVertical:10
          },
    isIcon:false,
    iconViewStyle:{},
    iconName:'',
    iconStyle:{},
    iconTextStyle:{},
    iconText:'',
    action:()=>{},
    spaceName:'',
    initialsTextStyle:{
      color:'white'
    },
    initials:'',
    isOpacity:0.2
  }

  constructor(props) {
    super(props);
  }

  componentDidMount(){
  }

  componentWillMount(){
  }

  render(){
    let {
      TouchableActiveSpaceCircle,
      activeSpaceTopCircle,
      circleWidth,
      spaceImage,
      circleText,
      spaceNumber,
      textStyle,
      imageStyle,
      containerStyle,
      textViewStyle,
      isIcon,
      iconViewStyle,
      iconName,
      iconStyle,
      iconTextStyle,
      iconText,
      action,
      spaceName,
      initialsTextStyle,
      initials,
      isOpacity
    }=this.props;
return(
  <View style={containerStyle}>
    <TouchableOpacity style={[{width:circleWidth,height:circleWidth,borderRadius:circleWidth/2},TouchableActiveSpaceCircle]}
      activeOpacity={isOpacity}
      onPress={()=>{
        action()
      }}
    >
      {(isIcon? (
        <View style={iconViewStyle}>
          <MyIcon name={iconName} style={iconStyle}/>
          <ScalableText style={iconTextStyle}>{iconText}</ScalableText>
        </View>
      ):(
        (spaceImage.uri ? (
          <Image
              style={imageStyle}
              source={spaceImage}
              resizeMode={'cover'}
              >
          </Image>
        ) : (
          <View style={[{width:circleWidth,height:circleWidth,borderRadius:circleWidth/2},TouchableActiveSpaceCircle]}>
            <ScalableText style={[initialsTextStyle]}>{initials}</ScalableText>
          </View>
        ))

      ))}
    </TouchableOpacity>

    {spaceNumber ? (
      <View style={activeSpaceTopCircle}>
        <Text style={circleText}>{spaceNumber}</Text>
      </View>
    ) : (
      <View/>
    )}

    <View style={textViewStyle}>
            <Text style={textStyle} numberOfLines={1} ellipsizeMode={'tail'} >
                {spaceName}
            </Text>
      </View>


  </View>
)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SpaceList)
